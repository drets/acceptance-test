Feature: Acceptance Test
  Test verifies that login, logout, account balance functionalities are working properly

  Scenario Outline: Acceptance Test scenario
    Given I have opened "<browserName>" browser
    And I have navigated to the "http://www.gameduell.com/" url
    And I have clicked New here? Test for free! link in account box
    When I have registered user by using following data:
      | Username  | Password | Your email        |
      | Uncle_Bob | Bob_1971 | bob@testemail.com |
    And I opened My GameDuell tab
    Then I have verified that the account balance in My GameDuell overview is the same like in account box
    When I have logout
    And I have login
    Then I have verified that I am on "/gd/emailManagement/emailValidation.xhtml" url
  Examples:
    | browserName |
    | chrome      |
    | firefox     |
