package com.gameduell.webdriver.core.forms;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author drets
 */
public class AccountBoxForm extends Form {

  @FindBy(css = "a[id=newHereLink]")
  private WebElement newHereLink;

  @FindBy(css = "a[id=loginLink]")
  private WebElement loginLink;

  @FindBy(css = "a[id=logoutLink]")
  private WebElement logoutLink;

  @FindBy(css = "a[title='Upgrade now'], a[title='Deposit funds']")
  private WebElement credits;

  @FindBy(css = "input[name='j_password']")
  private WebElement password;

  @FindBy(css = "input[name='j_username']")
  private WebElement username;

  public AccountBoxForm(WebDriver driver) {
    super(driver);
  }

  public RegistrationForm goToRegistrationForm(WebDriver driver) {
    newHereLink.click();
    return PageFactory.initElements(driver, RegistrationForm.class);
  }

  public void login(String user, String pass) {
    username.sendKeys(user);
    password.sendKeys(pass);
    submit();
  }

  public void clickLogout() {
    logoutLink.click();
  }

  public String getCredits() {
    return credits.getText();
  }

  @Override
  void submit() {
    loginLink.submit();
  }
}
