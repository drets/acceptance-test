package com.gameduell.webdriver.core.forms;

import com.gameduell.webdriver.core.utils.CommonExpectedConditions;
import com.gameduell.webdriver.core.utils.User;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author drets
 */
public class RegistrationForm extends Form {

  private static final String
      CORRECT_USERNAME_SELECTOR =
      "//input[@id='tf1']/following-sibling::span[@class='icon valid']";

  private static final String
      CORRECT_PASSWORD_SELECTOR =
      "//input[@id='tf2']/following-sibling::span[@class='icon valid']";

  private static final String
      CORRECT_EMAIL_SELECTOR =
      "//input[@id='tf3']/following-sibling::span[@class='icon valid']";

  @FindBy(css = "input[id=registrationButton]")
  private WebElement playForFree;

  @FindBy(css = "input[id=tf1]")
  private WebElement userName;

  @FindBy(css = "input[id=tf2]")
  private WebElement password;

  @FindBy(css = "input[id=tf3]")
  private WebElement email;

  @FindBy(css = "input[id=mailInput]")
  private WebElement mailInput;

  public RegistrationForm(WebDriver driver) {
    super(driver);
  }

  public User registerUser(String userNamePart, String pass, String emailAddressPart) {
    String userNameText = userNamePart + getRandomString();
    String emailText = getRandomString() + emailAddressPart;
    userName.sendKeys(userNameText);
    userName.sendKeys(Keys.TAB);
    password.sendKeys(pass);
    password.sendKeys(Keys.TAB);
    email.sendKeys(emailText);
    email.sendKeys(Keys.TAB);
    submit();
    CommonExpectedConditions.textToBePresent(mailInput, emailText);
    return new User(userNameText, pass);
  }


  private String getRandomString() {
    return Long.toHexString(Double.doubleToLongBits(Math.random())).substring(0, 7);
  }

  @Override
  void submit() {
    waitForElementByBy(By.xpath(CORRECT_USERNAME_SELECTOR));
    waitForElementByBy(By.xpath(CORRECT_PASSWORD_SELECTOR));
    waitForElementByBy(By.xpath(CORRECT_EMAIL_SELECTOR));
    playForFree.submit();
    // FIXME Change to a dynamic wait
    try {
      Thread.sleep(3000);
    } catch (Exception ignore) {
    }
  }
}
