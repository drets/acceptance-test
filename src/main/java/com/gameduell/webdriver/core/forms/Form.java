package com.gameduell.webdriver.core.forms;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Form {

  private static final int TIMEOUT_SECONDS = 10;

  protected WebDriver driver;

  public Form(WebDriver driver) {
    this.driver = driver;
  }

  public void waitForElementByBy(By by) {
    new WebDriverWait(driver, TIMEOUT_SECONDS)
        .until(ExpectedConditions.presenceOfElementLocated(by));
  }

  abstract void submit();
}