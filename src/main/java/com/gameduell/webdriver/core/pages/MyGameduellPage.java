package com.gameduell.webdriver.core.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * @author drets
 */
public class MyGameduellPage extends Page {

  @FindBy(xpath = "//p[text() = 'Account balance:']/span")
  private WebElement credits;

  public MyGameduellPage(WebDriver driver) {
    super(driver);
  }

  public String getCredits() {
    return credits.getText();
  }
}
