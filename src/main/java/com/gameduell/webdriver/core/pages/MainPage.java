package com.gameduell.webdriver.core.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * @author drets
 */
public class MainPage extends Page {

  @FindBy(css = "a[id=topNaviMygdLink]")
  private WebElement myGameduell;

  public MainPage(WebDriver driver) {
    super(driver);
  }

  public MyGameduellPage goToMyGameduellPage() {
    myGameduell.click();
    return PageFactory.initElements(driver, MyGameduellPage.class);
  }
}
