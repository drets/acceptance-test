package com.gameduell.webdriver.core.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class WebDriverFactory {

  public static final String CHROME = "chrome";
  public static final String FIREFOX = "firefox";

  public static WebDriver getDriver(Browser browser) {
    WebDriver driver = null;
    DesiredCapabilities capability = new DesiredCapabilities();
    capability.setJavascriptEnabled(true);
    String browserName = browser.name;
    if (browserName.equals(CHROME)) {
      setChromeDriver();
      driver = new ChromeDriver();
    } else if (browserName.equals(FIREFOX)) {
      driver = new FirefoxDriver();
    }
    return driver;
  }

  private static void setChromeDriver() {
    String chromeBinaryPath = "";
    String osName = System.getProperty("os.name").toUpperCase();

    if (osName.contains("WINDOWS")) {
      chromeBinaryPath = "ChromeDriver/chromedriver_win32/chromedriver.exe";
    } else if (osName.contains("MAC")) {
      chromeBinaryPath = "ChromeDriver/chromedriver_mac32/chromedriver";
    } else if (osName.contains("LINUX")) {
      chromeBinaryPath = " ChromeDriver/chromedriver_linux64/chromedriver";
    }
    try {
      setChromeDriver(chromeBinaryPath);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void setChromeDriver(String chromeBinaryPath) throws IOException {
    InputStream in = ClassLoader.getSystemResourceAsStream(chromeBinaryPath);
    String driverName = chromeBinaryPath.split("/")[2];
    Path pathToDriver = FileSystems.getDefault().getPath(System.getProperty("java.io.tmpdir"),
                                                         System.getProperty("file.separator"),
                                                         driverName);
    Files.copy(in, pathToDriver, StandardCopyOption.REPLACE_EXISTING);
    // set application user permissions to 455
    pathToDriver.toFile().setExecutable(true);
    System.setProperty("webdriver.chrome.driver", pathToDriver.toString());
  }
}