package com.gameduell.webdriver.core.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

/**
 * @author drets
 */
public class CommonExpectedConditions {

  private CommonExpectedConditions() {

  }

  /**
   * An expectation for checking if the given text is present in the specified element.
   */
  public static ExpectedCondition<Boolean> textToBePresent(
      final WebElement element, final String text) {

    return new ExpectedCondition<Boolean>() {
      public Boolean apply(WebDriver driver) {
        return element.getText().contains(text);
      }

      @Override
      public String toString() {
        return String.format("'%s' element has '%s' text", element.getTagName(), text);
      }
    };
  }
}
