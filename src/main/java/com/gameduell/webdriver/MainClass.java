package com.gameduell.webdriver;

import org.junit.runner.JUnitCore;

/**
 * @author drets
 */
public class MainClass {

  public static void main(String[] args) throws Exception {
    JUnitCore.main("com.gameduell.webdriver.RunTests");
  }
}
