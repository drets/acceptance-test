package com.gameduell.webdriver.steps;

import com.gameduell.webdriver.core.utils.Browser;
import com.gameduell.webdriver.core.utils.WebDriverFactory;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;

/**
 * @author drets
 */
public class GlobalSteps {

  public static WebDriver driver;
  protected Browser browser;
  private static final int TIMEOUT_SECONDS = 10;

  @Given("^I have opened \"([^\"]*)\" browser$")
  public void I_have_opened_browser(String browserName) throws Throwable {
    browser = new Browser(browserName);
    driver = WebDriverFactory.getDriver(browser);
    driver.manage().timeouts().implicitlyWait(TIMEOUT_SECONDS, TimeUnit.SECONDS);
    driver.manage().window().maximize();
  }

  @After
  public void afterScenario() {
    if (driver != null) {
      driver.quit();
    }
  }
}
