package com.gameduell.webdriver.steps;

import com.gameduell.webdriver.core.forms.AccountBoxForm;
import com.gameduell.webdriver.core.forms.RegistrationForm;
import com.gameduell.webdriver.core.pages.MainPage;
import com.gameduell.webdriver.core.pages.MyGameduellPage;
import com.gameduell.webdriver.core.utils.User;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * @author drets
 */
public class GameduellAcceptanceTest {

  private RegistrationForm registrationForm;
  private MyGameduellPage myGameduellPage;
  private AccountBoxForm accountBox;
  private MainPage mainPage;
  private User user;

  @Then("^I have verified that I am on \"([^\"]*)\" url$")
  public void I_have_verified_that_I_am_on_url(String urlPart) throws Throwable {
    String currentUrl = GlobalSteps.driver.getCurrentUrl();
    Assert.assertTrue(String.format("%s url doesn't contain %s", currentUrl, urlPart),
                      currentUrl.contains(urlPart));
  }

  @Given("^I have navigated to the \"([^\"]*)\" url$")
  public void I_have_navigated_to_the_url(String url) throws Throwable {
    GlobalSteps.driver.get(url);
  }

  @When("^I have registered user by using following data:$")
  public void I_have_registered_user_by_using_following_data(DataTable table) throws Throwable {
    List<Map<String, String>> userData = table.asMaps(String.class, String.class);
    String userNamePart = userData.get(0).get("Username");
    String password = userData.get(0).get("Password");
    String emailPart = userData.get(0).get("Your email");
    user = registrationForm.registerUser(userNamePart, password, emailPart);
  }

  @Then("^I have verified that the account balance in My GameDuell overview is the same like in account box$")
  public void I_have_verified_that_the_account_balance_in_My_GameDuell_overview_is_the_same_like_in_account_box() {
    String accountBalanceMyGameDuell = myGameduellPage.getCredits();
    String account = accountBox.getCredits();
    Assert.assertEquals(account, accountBalanceMyGameDuell);
  }

  @When("^I have logout$")
  public void I_have_logout() throws Throwable {
    accountBox = PageFactory.initElements(GlobalSteps.driver, AccountBoxForm.class);
    accountBox.clickLogout();
  }

  @And("^I have login$")
  public void I_have_login() throws Throwable {
    accountBox = PageFactory.initElements(GlobalSteps.driver, AccountBoxForm.class);
    accountBox.login(user.username, user.password);
  }

  @And("^I have clicked New here\\? Test for free! link in account box$")
  public void I_have_clicked_New_here_Test_for_free_link_in_account_box() throws Throwable {
    accountBox = PageFactory.initElements(GlobalSteps.driver, AccountBoxForm.class);
    registrationForm = accountBox.goToRegistrationForm(GlobalSteps.driver);
  }

  @And("^I opened My GameDuell tab$")
  public void I_opened_My_GameDuell_tab() throws Throwable {
    mainPage = PageFactory.initElements(GlobalSteps.driver, MainPage.class);
    myGameduellPage = mainPage.goToMyGameduellPage();
  }
}
